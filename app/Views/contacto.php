<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contacto</title>
    <link rel="stylesheet" href="/app/Css/estilo.css">
    <style>
        html {
            min-height: 100%;
            position: relative;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        body {
            font: 8pt Verdana, Geneva, Arial, Helvetica, sans-serif;
            text-align: center;
            margin: 0;
            background-color: white;

        }

        a:link, a:visited, a:active {
            text-decoration:none;
        }

        header{
            background-color: aquamarine;
            list-style-type: disc;
            text-align: right;
            margin: 0;
            padding: 0;
        }

        header li{
            display: inline-block;
            font-size: 20px;
            padding: 10px;
        }

        header li a{
            color: white;
        }
        header a:hover {

            color: black;
        }
        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            padding: 12px 16px;
            z-index: 1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .imagen:hover{
            border-radius:50%;
            -webkit-border-radius:50%;
            box-shadow: 0px 0px 15px 15px white;
            -webkit-box-shadow: 0px 0px 15px 15px white;
            transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
        }

        .contenido{
            margin-top: 5%;
            margin-left: 10%;
            margin-bottom: 5%;
            margin-right: 10%;
            margin: 10px;
            border: 1px solid #4CAF50;
        }
        .letras{
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
            color: black;
            font-weight: bold;
            border: 3px solid yellow;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
            width: 80%;
            padding: 20px;
            text-align: center;
        }
        footer {
            background-color: aquamarine;
            position: absolute;
            bottom: 0;
            width: 100%;
            color: white;
            text-align: center;
        }
    </style>
</head>
<body>
<p><h1>Blog Privado</h1></p>
<header>
    <li style="float: left;">
        <?php
        session_start();
        if (empty($_SESSION["Usuario"])){
            echo "<a href='index.php?controller=Usuario&action=login'>Iniciar sesion</a>";
        }else{
            echo "<p>".$_SESSION['Usuario']."<a href='index.php?controller=Usuario&action=logout'>  cerrar sesion</a></p>";
        }
        ?>
    </li>
    <li><a href="index.php?controller=Usuario&action=inicio">Inicio</a></li>
    <li><a href="index.php?controller=Usuario&action=novedades">Novedades</a></li>
    <li><a href="index.php?controller=Usuario&action=contacto">Contacto</a></li>
</header>
<main>
    <div class="contenido">
        <p class="letra">

        </p>
    </div>
</main>
<br><br><br><br>
<footer>
    <p>blog &copy; 2021 reservados</p>
</footer>
</body>
</html>