<?php
namespace UPT;

class Usuario extends Conexion
{
    public $id;
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $correo;
    public $edad;
    public $contrasenia;

    public $usuario;
    public $publica;
    public $novedad;

    public function __construct()
    {
        parent::__construct();
    }

    function crear(){
        $registro = mysqli_prepare($this->con,"INSERT INTO usuarios(nombres,apellido_paterno,apellido_materno,correo,edad,contrasenia) VALUES (?,?,?,?,?,?)");
        $registro->bind_param("ssssis",$this->nombre,$this->apellidoPaterno,$this->apellidoMaterno,$this->correo,$this->edad,$this->contrasenia);
        $registro->execute();
    }
    function publicacion(){
        $publico = mysqli_prepare($this->con,"INSERT INTO publicaciones(nombre,publicacion) VALUES (?,?)");
        $publico->bind_param("ss",$this->usuario,$this->publica);
        $publico->execute();
    }

    static function publicaciones(){
        $conexion = new Conexion();
        $pl = mysqli_prepare($conexion->con,"SELECT * FROM publicaciones");
        $pl->execute();
        while($resultado = mysqli_fetch_array($pl)){
                echo "  <tr><td width='150'>".$resultado['nombre']."</td>
                        <td width='150'>".$resultado['publicacion']."</td></tr>";
        }
    }
     function novedades(){
        $conexion = new Conexion();
        $nd = mysqli_prepare($conexion->con,"SELECT * FROM novedades");
        $nd->execute();
        $resultado = $nd->get_result();
        echo $resultado;
    }
    static function verificarUsuario($correo,$contrasenia){
        $conexion = new Conexion();
        $user = mysqli_prepare($conexion->con,"SELECT * FROM usuarios WHERE correo = ? AND contrasenia = ?");
        $user->bind_param("ss",$correo,$contrasenia);
        $user->execute();
        $resultado = $user->get_result();
        json_encode($resultado);
        return $resultado->fetch_object();
    }
    function novedad(){
        $fecha = new DateTime();
        $fecha->setDate(2001, 2, 3);
        $novedad = mysqli_prepare($this->con,"INSERT INTO novedades(nombre,novedad,fecha) VALUES(?,?,?)");
        $novedad->bind_param("sss",$this->usuario,$this->novedad,$fecha->format('Y-m-d'));
    }
}
