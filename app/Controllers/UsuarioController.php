<?php
require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';
use UPT\Usuario;
use UPT\Conexion;
class UsuarioController
{
    function registro(){
        require "app/Views/registro.php";
    }
    function inicio(){
        require "app/Views/home.php";
    }
    function novedades(){
        require "app/Views/novedades.php";
    }
    function contacto(){
        require "app/Views/contacto.php";
    }
    function verificaRegistro(){
        $usuario = new Usuario();
        $usuario->nombre = $_POST["nombre"];
        $usuario->apellidoPaterno = $_POST["apellidoPaterno"];
        $usuario->apellidoMaterno = $_POST["apellidoMaterno"];
        $usuario->correo = $_POST["correo"];
        $usuario->edad = $_POST["edad"];
        $usuario->contrasenia = $_POST["contrasenia"];
        $usuario->crear();
    }

    function login(){
        require "app/Views/login.php";
    }

    function verificarCredenciales(){
        if((!isset($_POST["correo"]) || (!isset($_POST["pass"])))){
            echo "Datos incorrectos";
            return false;
        }
        $correo = $_POST["correo"];
        $password = $_POST["pass"];
        $verificar = Usuario::verificarUsuario($correo,$password);
        if($verificar){
            session_start();
            $_SESSION["Usuario"]=$verificar->nombres;
            require "app/Views/home.php";

        }else{
            $estatus = "Datos incorrectos";
            require "app/Views/login.php";
        }
    }

    function logout(){
            session_start();
            unset($_SESSION["Usuario"]);
            $_SESSION["Usuario"]=false;
            require "app/Views/home.php";

    }
    function nuevapublicacion(){
        $nuevapl = new Usuario();
        session_start();
        $nuevapl->usuario=$_SESSION['Usuario'];
        $nuevapl->publica=$_POST['publico'];
        $nuevapl->publicacion();
        require "app/Views/home.php";
    }
    function nuevanovedad(){
        $nuevand = new Usuario();
        $nuevand->usuario=$_SESSION['Usuario'];
        $nuevand->novedad=$_POST['novedad'];
        $nuevand->novedad();
        require "app/Views/novedades.php";
    }
}